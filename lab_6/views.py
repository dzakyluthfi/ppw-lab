from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
    response['author'] = 'Luthfi Dzaky S'
    return render(request, 'lab_6/lab_6.html', response)
