from django.conf.urls import url
from .views import index, add_todo,delete_todolist

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^(\d+)$', delete_todolist, name='delete_todolist'),
]
